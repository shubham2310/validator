import * as validator from 'validator';
import { getParams } from '../lib/a.js';
interface Inner {
    valid:boolean|undefined,
    alpha?:boolean,
    alphanumeric?:boolean,
    boolean?:boolean,
    decimal?:boolean,
    email?:boolean,
    lowercase?:boolean,
    maxlength?:boolean,
    minlength?:boolean,
    numeric?:boolean,
    present?:boolean,
    regex?:boolean,
    uppercase?:boolean,
    text?:boolean
}
interface Outer {
    [key : string]: Inner,
}
interface Params {
    key:string,
    value:string|null|boolean,
    test:Tests
}
interface Tests {
    alpha?:boolean,
    alphanumeric?:boolean,
    boolean?:boolean,
    decimal?:boolean,
    email?:boolean,
    lowercase?:boolean,
    maxlength?:number,
    minlength?:number,
    numeric?:boolean,
    required?:boolean,
    regex?:RegExp,
    uppercase?:boolean,
    text?:boolean
}
export class validate {
    outer:Outer = {};
    isValid:boolean | undefined = true;

    constructor(){}

    validator(params:Params[]) : Object {
        
        params.forEach((param,index) => {
            let inner:Inner={
                valid:true
            };
            this.outer[param.key]=inner;
            if(param.test.hasOwnProperty('alpha')){
                if(param.test.alpha===true){
                    if(typeof(param.value)==='string')
                        this.outer[param.key].alpha=validator.isAlpha(param.value);
                    else
                        this.outer[param.key].alpha=false;
                    this.outer[param.key].valid=this.outer[param.key].valid&&this.outer[param.key].alpha;
                }
            }
            if(param.test.hasOwnProperty('alphanumeric')){
                if(param.test.alphanumeric===true){
                    if(typeof(param.value)==='string')
                        this.outer[param.key].alphanumeric=validator.isAlphanumeric(param.value);
                    else
                        this.outer[param.key].alphanumeric=false;
                    this.outer[param.key].valid=this.outer[param.key].valid&&this.outer[param.key].alphanumeric;
                }
            }
            if(param.test.hasOwnProperty('boolean')){
                if(param.test.boolean===true){
                    if(typeof(param.value)==='string')
                        this.outer[param.key].boolean=validator.isBoolean(param.value);
                    else if(typeof(param.value)==='boolean')
                        this.outer[param.key].boolean=true;
                    else
                        this.outer[param.key].boolean=false;
                    this.outer[param.key].valid=this.outer[param.key].valid&&this.outer[param.key].boolean;
                }
            }
            if(param.test.hasOwnProperty('decimal')){
                if(param.test.decimal===true){
                    if(typeof(param.value)==='string')
                        this.outer[param.key].decimal=validator.isDecimal(param.value);
                    else
                        this.outer[param.key].decimal=false;
                    this.outer[param.key].valid=this.outer[param.key].valid&&this.outer[param.key].decimal;
                }
            }
            if(param.test.hasOwnProperty('email')){
                if(param.test.email===true){
                    if(typeof(param.value)==='string')
                        this.outer[param.key].email=validator.isEmail(param.value);
                    else
                        this.outer[param.key].email=false;
                    this.outer[param.key].valid=this.outer[param.key].valid&&this.outer[param.key].email;
                }
            }
            if(param.test.hasOwnProperty('lowercase')){
                if(param.test.lowercase===true){
                    if(typeof(param.value)==='string')
                        this.outer[param.key].lowercase=validator.isLowercase(param.value);
                    else
                        this.outer[param.key].lowercase=false;
                    this.outer[param.key].valid=this.outer[param.key].valid&&this.outer[param.key].lowercase;
                }
            }
            if(param.test.hasOwnProperty('maxlength')){
                if(typeof(param.test.maxlength)=='number'){
                    if(typeof(param.value)==='string')
                        this.outer[param.key].maxlength=validator.isLength(param.value,{max:param.test.maxlength});
                    else
                        this.outer[param.key].maxlength=false;
                    this.outer[param.key].valid=this.outer[param.key].valid&&this.outer[param.key].maxlength;
                }
            }
            if(param.test.hasOwnProperty('minlength')){
                if(typeof(param.test.minlength)=='number'){
                    if(typeof(param.value)==='string')
                        this.outer[param.key].minlength=validator.isLength(param.value,{min:param.test.minlength});
                    else
                        this.outer[param.key].minlength=false;
                    this.outer[param.key].valid=this.outer[param.key].valid&&this.outer[param.key].minlength;
                }
            }
            if(param.test.hasOwnProperty('numeric')){
                if(param.test.numeric===true){
                    if(typeof(param.value)==='string')
                        this.outer[param.key].numeric=validator.isNumeric(param.value);
                    else
                        this.outer[param.key].numeric=false;
                    this.outer[param.key].valid=this.outer[param.key].valid&&this.outer[param.key].numeric;
                }
            }
            if(param.test.hasOwnProperty('regex')){
                if(param.test.regex instanceof RegExp){
                    if(typeof(param.value)==='string'){
                        this.outer[param.key].regex=(param.test.regex).test(param.value);
                    }
                    else
                        this.outer[param.key].regex=false;
                    this.outer[param.key].valid=this.outer[param.key].valid&&this.outer[param.key].regex;
                }
            }
            if(param.test.hasOwnProperty('required')){
                if(param.test.required===true){
                    if(param.value==null)
                        this.outer[param.key].present=false;
                    else if(typeof(param.value)==='boolean')
                        this.outer[param.key].present=true;
                    else
                        this.outer[param.key].present=(!validator.isEmpty(param.value));
                    this.outer[param.key].valid=this.outer[param.key].valid&&this.outer[param.key].present;
                }
            }
            if(param.test.hasOwnProperty('uppercase')){
                if(param.test.uppercase===true){
                    if(typeof(param.value)==='string')
                        this.outer[param.key].uppercase=validator.isUppercase(param.value);
                    else
                        this.outer[param.key].uppercase=false;
                    this.outer[param.key].valid=this.outer[param.key].valid&&this.outer[param.key].uppercase;
                }
            }
            if(param.test.hasOwnProperty('text')){
                if(param.test.text===true){
                    if(typeof(param.value)==='string')
                        this.outer[param.key].text=validator.isAlpha(validator.blacklist(param.value,' '));
                    else
                        this.outer[param.key].text=false;
                    this.outer[param.key].valid=this.outer[param.key].valid&&this.outer[param.key].text;
                }
            }
            this.isValid=this.isValid&&this.outer[param.key].valid;
        });
        return this.outer;
    }
}
let val:validate = new validate();
let obj=val.validator(getParams());
console.log(obj);