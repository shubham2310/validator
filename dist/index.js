"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var validator = require("validator");
var a_js_1 = require("../lib/a.js");
var validate = /** @class */ (function () {
    function validate() {
        this.outer = {};
        this.isValid = true;
    }
    validate.prototype.validator = function (params) {
        var _this = this;
        params.forEach(function (param, index) {
            var inner = {
                valid: true
            };
            _this.outer[param.key] = inner;
            if (param.test.hasOwnProperty('alpha')) {
                if (param.test.alpha === true) {
                    if (typeof (param.value) === 'string')
                        _this.outer[param.key].alpha = validator.isAlpha(param.value);
                    else
                        _this.outer[param.key].alpha = false;
                    _this.outer[param.key].valid = _this.outer[param.key].valid && _this.outer[param.key].alpha;
                }
            }
            if (param.test.hasOwnProperty('alphanumeric')) {
                if (param.test.alphanumeric === true) {
                    if (typeof (param.value) === 'string')
                        _this.outer[param.key].alphanumeric = validator.isAlphanumeric(param.value);
                    else
                        _this.outer[param.key].alphanumeric = false;
                    _this.outer[param.key].valid = _this.outer[param.key].valid && _this.outer[param.key].alphanumeric;
                }
            }
            if (param.test.hasOwnProperty('boolean')) {
                if (param.test.boolean === true) {
                    if (typeof (param.value) === 'string')
                        _this.outer[param.key].boolean = validator.isBoolean(param.value);
                    else if (typeof (param.value) === 'boolean')
                        _this.outer[param.key].boolean = true;
                    else
                        _this.outer[param.key].boolean = false;
                    _this.outer[param.key].valid = _this.outer[param.key].valid && _this.outer[param.key].boolean;
                }
            }
            if (param.test.hasOwnProperty('decimal')) {
                if (param.test.decimal === true) {
                    if (typeof (param.value) === 'string')
                        _this.outer[param.key].decimal = validator.isDecimal(param.value);
                    else
                        _this.outer[param.key].decimal = false;
                    _this.outer[param.key].valid = _this.outer[param.key].valid && _this.outer[param.key].decimal;
                }
            }
            if (param.test.hasOwnProperty('email')) {
                if (param.test.email === true) {
                    if (typeof (param.value) === 'string')
                        _this.outer[param.key].email = validator.isEmail(param.value);
                    else
                        _this.outer[param.key].email = false;
                    _this.outer[param.key].valid = _this.outer[param.key].valid && _this.outer[param.key].email;
                }
            }
            if (param.test.hasOwnProperty('lowercase')) {
                if (param.test.lowercase === true) {
                    if (typeof (param.value) === 'string')
                        _this.outer[param.key].lowercase = validator.isLowercase(param.value);
                    else
                        _this.outer[param.key].lowercase = false;
                    _this.outer[param.key].valid = _this.outer[param.key].valid && _this.outer[param.key].lowercase;
                }
            }
            if (param.test.hasOwnProperty('maxlength')) {
                if (typeof (param.test.maxlength) == 'number') {
                    if (typeof (param.value) === 'string')
                        _this.outer[param.key].maxlength = validator.isLength(param.value, { max: param.test.maxlength });
                    else
                        _this.outer[param.key].maxlength = false;
                    _this.outer[param.key].valid = _this.outer[param.key].valid && _this.outer[param.key].maxlength;
                }
            }
            if (param.test.hasOwnProperty('minlength')) {
                if (typeof (param.test.minlength) == 'number') {
                    if (typeof (param.value) === 'string')
                        _this.outer[param.key].minlength = validator.isLength(param.value, { min: param.test.minlength });
                    else
                        _this.outer[param.key].minlength = false;
                    _this.outer[param.key].valid = _this.outer[param.key].valid && _this.outer[param.key].minlength;
                }
            }
            if (param.test.hasOwnProperty('numeric')) {
                if (param.test.numeric === true) {
                    if (typeof (param.value) === 'string')
                        _this.outer[param.key].numeric = validator.isNumeric(param.value);
                    else
                        _this.outer[param.key].numeric = false;
                    _this.outer[param.key].valid = _this.outer[param.key].valid && _this.outer[param.key].numeric;
                }
            }
            if (param.test.hasOwnProperty('regex')) {
                if (param.test.regex instanceof RegExp) {
                    if (typeof (param.value) === 'string') {
                        _this.outer[param.key].regex = (param.test.regex).test(param.value);
                    }
                    else
                        _this.outer[param.key].regex = false;
                    _this.outer[param.key].valid = _this.outer[param.key].valid && _this.outer[param.key].regex;
                }
            }
            if (param.test.hasOwnProperty('required')) {
                if (param.test.required === true) {
                    if (param.value == null)
                        _this.outer[param.key].present = false;
                    else if (typeof (param.value) === 'boolean')
                        _this.outer[param.key].present = true;
                    else
                        _this.outer[param.key].present = (!validator.isEmpty(param.value));
                    _this.outer[param.key].valid = _this.outer[param.key].valid && _this.outer[param.key].present;
                }
            }
            if (param.test.hasOwnProperty('uppercase')) {
                if (param.test.uppercase === true) {
                    if (typeof (param.value) === 'string')
                        _this.outer[param.key].uppercase = validator.isUppercase(param.value);
                    else
                        _this.outer[param.key].uppercase = false;
                    _this.outer[param.key].valid = _this.outer[param.key].valid && _this.outer[param.key].uppercase;
                }
            }
            if (param.test.hasOwnProperty('text')) {
                if (param.test.text === true) {
                    if (typeof (param.value) === 'string')
                        _this.outer[param.key].text = validator.isAlpha(validator.blacklist(param.value, ' '));
                    else
                        _this.outer[param.key].text = false;
                    _this.outer[param.key].valid = _this.outer[param.key].valid && _this.outer[param.key].text;
                }
            }
            _this.isValid = _this.isValid && _this.outer[param.key].valid;
        });
        return this.outer;
    };
    return validate;
}());
exports.validate = validate;
var val = new validate();
var obj = val.validator(a_js_1.getParams());
console.log(obj);
