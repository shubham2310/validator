"use strict";
module.exports = {
    getParams: function () {
        var params = [
            {
                "key": "email",
                "value": "shubham.libra23@gmail.com",
                "test": {
                    "specialCharacters": ['@', '3', '!'],
                    "required": true,
                    "maxlength": 10,
                    "minlength": 8,
                    "alphanumeric": true,
                    "regex": /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
                }
            },
            {
                "key": "name",
                "value": "Ankur Sharma",
                "test": {
                    "required": true,
                    "text": true,
                    "alpha": true,
                }
            },
            {
                "key": "organization",
                "value": "Technocube",
                "test": {
                    "required": true,
                    "alpha": true,
                }
            },
            {
                "key": "password",
                "value": "123@f_49",
                "test": {
                    "required": true,
                    "minlength": 8,
                    "maxlength": 12,
                }
            },
            {
                "key": "checkbox",
                "value": null,
                "test": {
                    "required": true,
                    "boolean": true
                }
            }
        ];
        return params;
    }
};
